/*
 * SPI_M.h
 *
 *  Author: MaxCiv
 */ 
#ifndef SPI_M_H_
#define SPI_M_H_

#define SPI_DDR DDRB
#define SPI_PORT PORTB

#define SPI_MOSI_BIT PB3	// MOSI 3
#define SPI_MISO_BIT PB4	// MISO 4
#define SPI_SCK_BIT PB5		// SCK  5
#define SPI_CS_BIT PB2		// SS   2

#define SPIMODE0 (0<<CPHA)
#define SPIMODE1 (1<<CPHA)
#define SPIMODE2 (2<<CPHA)
#define SPIMODE3 (3<<CPHA)

#define LSB_FIRST (1<<DORD)
#define MSB_FIRST (0<<DORD)

#define CLOCKDIV4 (0<<SPR0)
#define CLOCKDIV16 (1<<SPR0)
#define CLOCKDIV64 (2<<SPR0)

void spi_init(uint8_t setup);
void spi_send_byte(uint8_t data);
uint8_t spi_read_byte(void);

void spi_init(uint8_t setup){
	SPI_DDR |=  (1<<SPI_MOSI_BIT)|(1<<SPI_SCK_BIT)|(1<<SPI_CS_BIT)|(0<<SPI_MISO_BIT);	// ��, ����� MISO, �� �����
	SPI_PORT |= (1<<SPI_MOSI_BIT)|(1<<SPI_SCK_BIT)|(1<<SPI_CS_BIT)|(1<<SPI_MISO_BIT);

	//SPCR = (1<<SPE)|(1<<MSTR)|setup;
	
	// ���������� spi,������� ��� ������,������, ����� 0, f/16
	SPCR = (1<<SPE)|(0<<DORD)|(1<<MSTR)|(0<<CPOL)|(0<<CPHA)|(0<<SPR1)|(1<<SPR0);
	SPSR = (0<<SPI2X);
}

void spi_send_byte(uint8_t data){
	SPDR = data;
	while(!(SPSR & (1<<SPIF)));	// �������� 
}

uint8_t spi_read_byte(void){
	SPDR = 0x00;
	while(!(SPSR & (1<<SPIF)));
	return SPDR;
}
#endif

/*
#ifndef SPI_M_H_
#define SPI_M_H_

#define SPI_DDR DDRB
#define SPI_MOSI_BIT PB3	// MOSI 3
#define SPI_SCK_BIT PB5		// SCK  5
#define SPI_CS_BIT PB2		// SS   2

#define SPIMODE0 (0<<CPHA)
#define SPIMODE1 (1<<CPHA)
#define SPIMODE2 (2<<CPHA)
#define SPIMODE3 (3<<CPHA)

#define LSB_FIRST (1<<DORD)
#define MSB_FIRST (0<<DORD)

#define CLOCKDIV4 (0<<SPR0)
#define CLOCKDIV16 (1<<SPR0)
#define CLOCKDIV64 (2<<SPR0)

void spi_init(uint8_t setup);
void spi_send_byte(uint8_t data);
uint8_t spi_read_byte(void);

void spi_init(uint8_t setup){
	SPI_DDR |= (1<<SPI_MOSI_BIT)|(1<<SPI_SCK_BIT)|(1<<SPI_CS_BIT);

	SPCR = (1<<SPE)|(1<<MSTR)|setup;
}

void spi_send_byte(uint8_t data){
	SPDR = data;
	while(!(SPSR & (1<<SPIF)));
}

uint8_t spi_read_byte(void){
	SPDR = 0x00;
	while(!(SPSR & (1<<SPIF)));
	return SPDR;
}
#endif
*/